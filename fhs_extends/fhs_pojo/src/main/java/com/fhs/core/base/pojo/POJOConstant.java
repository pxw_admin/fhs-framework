package com.fhs.core.base.pojo;

/**
 * POJO 常量
 */
public interface POJOConstant {

    String EQ = "0";

    String LIKE = "1";

    String NEQ = "2";

    String BIGGER_EQ = "3";

    String LESS_EQ = "4";

    String BIGGER = "5";

    String LESS = "6";

    String START_WITH = "7";

    String END_WITH = "8";


}
