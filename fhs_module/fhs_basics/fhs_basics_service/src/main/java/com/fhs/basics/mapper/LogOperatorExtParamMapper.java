package com.fhs.basics.mapper;

import com.fhs.basics.po.LogOperatorExtParamPO;
import com.fhs.core.base.mapper.FhsBaseMapper;
import org.springframework.stereotype.Repository;

/**
 * 扩展参数(LogOperatorExtParam)表数据库访问层
 *
 * @author wanglei
 * @since 2020-04-23 13:58:59
 */
@Repository
public interface LogOperatorExtParamMapper extends FhsBaseMapper<LogOperatorExtParamPO> {

}
