package com.fhs.basics.mapper;

import com.fhs.basics.po.UcenterMsTenantPO;
import com.fhs.core.base.mapper.FhsBaseMapper;

/**
 * 租户管理(UcenterMsTenant)表数据库访问层
 *
 * @author makejava
 * @since 2019-05-15 14:21:04
 */
public interface UcenterMsTenantMapper extends FhsBaseMapper<UcenterMsTenantPO> {

}
