package com.fhs.basics.mapper;

import com.fhs.core.base.mapper.FhsBaseMapper;
import com.fhs.basics.po.OrderNumberPO;

/**
 * @author jianbo.qin
 * @version [版本号, 2018/05/10 15:09:42]
 * @Description:生成订单号
 * @versio 1.0
 * 陕西小伙伴网络科技有限公司
 * Copyright (c) 2017 All Rights Reserved.
 */
public interface OrderNumberMapper extends FhsBaseMapper<OrderNumberPO> {
}
