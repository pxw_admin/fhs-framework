package com.fhs.basics.service;

import com.fhs.basics.po.UcenterMsTenantPO;
import com.fhs.basics.vo.UcenterMsTenantVO;
import com.fhs.core.base.service.BaseService;

/**
 * 租户管理(UcenterMsTenant)表服务接口
 *
 * @author makejava
 * @since 2019-05-15 14:21:04
 */
public interface UcenterMsTenantService extends BaseService<UcenterMsTenantVO, UcenterMsTenantPO> {


}