package com.fhs.basics.constant;

import com.fhs.core.trans.constant.TransType;

/**
 * 翻译服务静态常量
 *
 * @author user
 * @date 2020-05-18 14:42:20
 */
public interface BaseTransConstant {
    /**
     * 机构信息
     */
    String SYS_ORGANIZATION_INFO = "sysOrganizationInfo";


    /**
     * 用户信息
     */
    String USER_INFO = "sysUser";

    /**
     * 角色管理
     */
    String ROLE_INFO = "sysRole";

    /**
     * 菜单管理
     */
    String MENU_INFO = "sysMenu";
    /**
     * 菜单权限管理
     */
    String MENU_INFO_PERMISSION = "sysMenuPermission";
    /**
     * 地区
     */
    String AREA = "area";

    /**
     * 机构名称
     */
    String SYS_ORGANIZATION_NAME = "sysOrganizationInfo:organizationName";


    /**
     * 区域字典
     */
    String AREA_NAME = "area:areaName";

    /**
     * 组织机构
     */
    String ORG = "org";
}
