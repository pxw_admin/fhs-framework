package com.fhs.basics.constant;

public interface OrgConstant {
    /**
     * 最顶级节点
     */
    String ORG_ID_ROOT = "001";

    /**
     * 顶级的本部
     */
    String ORG_ROOT_COMPANY_REAL = "001b";
}
