

本项目是一个基于vite2.0 + vue2.x + elementUI2.x 的后台管理系统模板。


### 安装依赖

```bash
npm install
```
### 启动本地开发环境（带热启动）

```bash
npm run dev
```
### 构建生产环境 (带压缩)

```bash
npm run build
```
### 生产环境预览

```bash
npm run serve
```


