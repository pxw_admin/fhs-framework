/**
 * crud:控制crud组件加载和销毁
 * isEdit：是否编辑
 * open:默认表单弹窗状态
 * init:编辑的时候的初始化数据
 */

export default {
  data(){
    return{
      crud:false,
      isEdit:false,
      isDetail:false,
      open:false,
      init: {},
      title:undefined
    }
  },
  methods:{


  }

}
