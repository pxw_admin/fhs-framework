export const deepClone = (source)=>{
  if (!source && typeof source !== 'object') {
    throw new Error('error arguments', 'deepClone')
  }
  const targetObj = source.constructor === Array ? [] : {}
  Object.keys(source).forEach(keys => {
    if (source[keys] && typeof source[keys] === 'object') {
      targetObj[keys] = deepClone(source[keys])
    } else {
      targetObj[keys] = source[keys]
    }
  })
  return targetObj
}

/**
 * Parse the time to string
 * @param {(Object|string|number)} time
 * @param {string} cFormat
 * @returns {string}
 */
export function parseTime(time, cFormat) {
  if (arguments.length === 0) {
    return null
  }
  const format = cFormat || '{y}-{m}-{d} {h}:{i}:{s}'
  let date
  if (typeof time === 'object') {
    date = time
  } else {
    if ((typeof time === 'string') && (/^[0-9]+$/.test(time))) {
      time = parseInt(time)
    }
    if ((typeof time === 'number') && (time.toString().length === 10)) {
      time = time * 1000
    }
    date = new Date(time)
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  }
  const time_str = format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
    let value = formatObj[key]
    // Note: getDay() returns 0 on Sunday
    if (key === 'a') { return ['日', '一', '二', '三', '四', '五', '六'][value ] }
    if (result.length > 0 && value < 10) {
      value = '0' + value
    }
    return value || 0
  })
  return time_str
}
/*
*@param Object; 如需添加operation属性 在key中以$分隔，如 {'age$>':18},operation 默认为“=”
*@returns Object;
*/
export function formatSeachParams(obj){
  let params = {
    "groupRelation": "AND",
    "querys":[]
  }
  let objKey = Object.keys(obj);
  objKey.forEach(item => {
    let itemObj = {
        "group": "main",
        "operation": item.indexOf('$') > 0 ? item.substring(item.indexOf('$')+1) :"=",
        "property":  item.indexOf('$') > 0 ? item.substring(0,item.indexOf('$')) :item,
        "relation": "AND",
        "value": obj[item]
    };
    params.querys.push(itemObj);
  });
  return params;
}
